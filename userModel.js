const mongoose = require("mongoose");

//user personal info 
const imgSchema = {
	name : String,
	img :{
		data: Buffer,
		contentType : String
	}
};

const userEducation = {
	school  : String,
	passingyear : String,
	College : String,
	college_passing_year : String,
	major : String
}

const userAddress = {
	localAddress :String,
	country  : String,
	city     : String,
	zip      : String
}

const additionalSchema = {
	dob :Date,
	address : userAddress,
	imgData : imgSchema,
	about  : String,
	education : userEducation,
	job :  String
}

//MAIN SCHEMA FOR USER 

const userSchema = new mongoose.Schema({
	uid    : String,
	uname  : String,
	lname  : String,
	mobile : String,
	email  : String,
	pwd    : String,
	Admin  : Boolean,
	Manager : Boolean,
	add    : additionalSchema
});


//all user char data
// const chatArray = {			//just for reference 
// 	sender : String,
// 	rec : String,
// 	message : String
// }

const chatOwner = new mongoose.Schema({
	owner1 : String,
	owner2 : String,
	chats : Array
});

const paymentSchema = {
	amount  : Number			//Update this schema when the user payment mode is selected and the options are provided 
}

const platformSchema = {
	venue : String , 
	online  : Boolean
}


//PUSUEDO SCHEMA UPDATE WHILE WORKING ON THE INVITE FROM USER 
const attendedSchema = [{
	name : String,
	attenedId :  String,
	invitationCode : String
}]

const eventsSchema =  new mongoose.Schema({
	name  : String,
	owner : String , 
	hostName : String,
	members : Number,
	platform : platformSchema,
	paid : Boolean,
	price : Number,
	startDate : Date,
	expiry : Date,
	payment : paymentSchema,
	attendents : attendedSchema
});

const chats = new mongoose.model('chatOwner',  chatOwner);
const user = new mongoose.model('userData' , userSchema);

module.exports = {
	users : user,
	chat : chats
} 