const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express.Router();
const Users = require('../userModel.js').users;
const isLoggedIn = require("../middleware/session-handler").log;

app.route('/users/:uid')
.get(isLoggedIn,(req , res)=>{
	//if The user is not logged in throw the user to login page 
	if(!req.logged){
		res.redirect("/");
	}
	else{
		const uid = req.params.uid;
		Users.findById(uid, (err , results)=>{
			if(results){
				Users.find({} ,(err , allUsers)=>{
				if(results){
				res.render("users", 
					{
						user : results,
						emp : allUsers
					});
				}
				})

			}else{
				res.send("Something went wrong 1");
			}
		})
	}
	
	
})

module.exports = {
	routes : app
}