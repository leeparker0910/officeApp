require("dotenv").config()
const sessions = require("express-session");
const Users = require('../userModel').users

const oneDay = 1000*60*60*2;			//2 days
const sessionConfig = {
	secret  : process.env.SECRET,
	saveUninitialized:true,
    cookie: { maxAge: oneDay },
    resave: false
};

var session;


//////CHECKS FOR SESSION 
const isLoggedIn = (req ,res  , next)=>{
	session = req.session;
	if(session.userid){
		req.user = session.userid;
		req.logged = true;
		next();
	}else{
		req.user = session.userid;
		req.logged = false;
		next();
	}
}


//////RESPOSINBLE FOR SETTING USER SESSION EACH TIME NEW USER IS LOGGED IN
const login = (req , res , next)=>{
	const user = req.user;
	req.session.userid = user;
	req.user = user;
	next()
}

const logout = (req ,res , next)=>{
	const user = req.user;
	req.session.destroy();
	res.redirect("/");
}

module.exports = {
	config : sessionConfig,
	log    : isLoggedIn,
	userLogin : login,
	logout : logout
}